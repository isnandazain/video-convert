from flask import Flask, request, jsonify, send_file

from configuration import ProsaConfig
from pathlib import Path
from werkzeug.utils import secure_filename
from concurrent.futures import ThreadPoolExecutor

import random
import string
import os
import subprocess
import shlex

app = Flask(__name__)
app.config.from_object(ProsaConfig)

def _os_execute(command):
    result, err = subprocess.Popen(
        shlex.split(command),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE).communicate()
    return result, err


@app.route("/files/<path:path_file>", methods=["GET"])
def get_file(path_file):
    str_path_file = ""
    for x in path_file.split("/"):
        str_path_file = str_path_file + "/" + x

    filename = path_file.split("/")[-1]

    upload_dir = str(ProsaConfig.STORAGE_PATH)
    file_send = upload_dir + "/" + str_path_file

    return send_file(file_send, attachment_filename=filename)


@app.route("/convert", methods=['POST'])
def request_convert():
    preset = request.form.get("preset")
    video = request.files.get("video")

    if not preset:
        return jsonify({"status_code": 400, "message": "preset masih kosong"}), 400

    if not video:
        return jsonify({"status_code": 400, "message": "video masih kosong"}), 400

    if preset not in ProsaConfig.VIDEO_PRESET:
        return jsonify({"status_code": 400, 
                        "message": "preset tidak ditemukan, gunakan preset yang lain"}), 400

    # convert video
    filename = save_video(video)
    result_filename = convert_video(filename, preset)

    video_url = []
    for res in result_filename:
        video_url.append("http://127.0.0.1:5000/files/" + res)

    response = {
        "status": 200,
        "videos": video_url
    }

    return jsonify(response)


def convert_video(filename, preset):
    upload_dir = Path(ProsaConfig.STORAGE_PATH)

    # untuk separate filename dan ext
    name, ext = os.path.splitext(filename)
    video_ratio = ProsaConfig.VIDEO_RATIO

    with ThreadPoolExecutor() as executor:
        # generate filename
        filename_ratio1 = "%s_%s%s" % (name, video_ratio[0], ext)
        filename_ratio2 = "%s_%s%s" % (name, video_ratio[1], ext)
        filename_ratio3 = "%s_%s%s" % (name, video_ratio[2], ext)

        # execute ffmpeg
        task_ratio1 = executor.submit(_os_execute('ffmpeg -i {} -c:a copy -preset {} {}'.format( str(upload_dir)+"/"+filename, preset, str(upload_dir)+"/"+filename_ratio1 )))
        task_ratio2 = executor.submit(_os_execute('ffmpeg -i {} -c:a copy -preset {} {}'.format( str(upload_dir)+"/"+filename, preset, str(upload_dir)+"/"+filename_ratio2 )))
        task_ratio3 = executor.submit(_os_execute('ffmpeg -i {} -c:a copy -preset {} {}'.format( str(upload_dir)+"/"+filename, preset, str(upload_dir)+"/"+filename_ratio3 )))

    return [filename_ratio1, filename_ratio2, filename_ratio3]

def save_video(file):
    upload_dir = Path(ProsaConfig.STORAGE_PATH)

    # save file original
    # get filename
    filename = file.filename

    # make sure filename is safe
    filename = safe_filename(filename)
    while (upload_dir / filename).is_file():
        filename = safe_filename(file.filename)

    file.save(str(upload_dir / filename))
    file.close()

    return filename


def safe_filename(filename, maxchar=35):
    name, ext = os.path.splitext(filename)

    random_str = "_" + "".join(
        random.choice(string.ascii_uppercase + string.ascii_lowercase) for _ in range(10)
    )

    name_max = maxchar - len(ext) - len(random_str)
    if len(name) > name_max:
        name = name[:name_max]

    name = "%s%s%s" % (name, random_str, ext)

    return secure_filename(name)


if __name__ == '__main__':
    app.run(debug=True)