# Convert Video API

## Install Package
pip install -r requirements.txt

## Run
python app.py

# URL
**/convert** <br/>
parameter :
- preset (preset convert yang diinginkan)
- video (file video yang akan di convert)

## Test
python -m pytest