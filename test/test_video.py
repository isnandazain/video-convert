def test_convert_video(client):
    video = open("nickyromero_stay.mp4", "rb", buffering=0)

    response = client.post("http://127.0.0.1:5000/convert", data={
        "preset": "superfast",
        "video": video,
    })

    response = response.json()
    assert response['status'] == 200
    assert len(response['videos']) == 3