import pytest
import json
from flask.wrappers import Response

from app import app
from configuration import ProsaConfig

class ProsaResponse(Response):
    def json(self) -> dict:
        return json.loads(self.get_data(as_text=True))

@pytest.fixture
def context():
    _context = app
    _context.response_class = ProsaResponse
    _context.app_context().push()
    return _context

@pytest.fixture
def client(context):
    return context.test_client()