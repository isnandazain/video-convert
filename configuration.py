import os


def getenv(key, default=None, func=None):
    val = os.getenv(key, default)
    if func:
        val = func(val)
    return val

class ProsaConfig(object):
    # assets directory
    STORAGE_PATH = getenv("STORAGE_PATH", "file")

    # video format
    VIDEO_PRESET = ['veryslow', 'slow', 'medium', 'fast', 'faster', 'veryfast', 'superfast']
    VIDEO_FORMAT = ['mp4', 'avi', 'mkv']
    VIDEO_RATIO = ['90', '70', '50']

    # database config
    MYSQL_HOST = getenv('DB_HOST', '127.0.0.1')
    MYSQL_USER = getenv('DB_USER', 'root')
    MYSQL_PASS = getenv('DB_PASS', '')
    MYSQL_DBNAME = getenv('DB_NAME', 'prosaai')
    MYSQL_PORT = getenv('DB_PORT', 3306, int)

    # sqlalchemy
    SQLALCHEMY_DATABASE_URI = "mysql://%s:%s@%s:%s/%s?charset=utf8mb4" % (
        MYSQL_USER, MYSQL_PASS, MYSQL_HOST, MYSQL_PORT, MYSQL_DBNAME
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = getenv('SQLALCHEMY_TRACK_MODIFICATIONS', False, bool)
    SQLALCHEMY_POOL_SIZE = 10
    SQLALCHEMY_POOL_RECYCLE = 60
    SQLALCHEMY_MAX_OVERFLOW = 20

    LINEBOT_CHANNEL_ACCESS_TOKEN = "this for line channel access token"
    LINEBOT_CHANNEL_SECRET = "this for line channel secret"